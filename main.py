from nicegui import ui
import asyncio

mydata = []
selected_data = []
rows = []
columns = []

ui.table(title='Телефонный справочник', columns=columns, rows=rows)

mytable = ui.aggrid({
    'columnDefs': [
        {'headerName': 'Подразделение', 'field': 'name'},
        {'headerName': 'Должность', 'field': 'age'},
        {'headerName': 'ФИО', 'field': 'fullname'},
        {'headerName': 'Номер гор. тел.', 'field': 'phone'},
        {'headerName': 'Номер сот. тел.', 'field': 'mobilephone'},
        
    ],
    'rowData': [],
    'rowSelection': 'multiple',
})

#ДОБАВЛЕНИЕ ДАННЫХ В ТАБЛИЦУ
def addnewdata():
    mydata.append({"name":add_name.value, 
                   "age":add_age.value, 
                   "fullname":add_fullname.value, 
                   "phone":add_phone.value, 
                   "mobilephone":add_mobilephone.value})
    
    mytable.options['rowData'] = sorted(mydata, key=lambda data:data['name'])
    ui.notify("Добавлен")
    new_data_dialog.close()
    mytable.update()

#ДИАЛОГ ДЛЯ ДОБАВЛЕНИЯ НОВЫХ ДАННЫХ
with ui.dialog() as new_data_dialog:
      with ui.card():
            ui.label('Новая карточка')
            add_name = ui.input(label="Подразделение")
            add_age = ui.input(label="Должность")
            add_fullname = ui.input(label="ФИО")
            add_phone = ui.input(label="Номер телефона")
            add_mobilephone = ui.input(label="Номер моб. тел.")

            ui.button("Добавить карточку", on_click=addnewdata)

def opendata(e):
    new_data_dialog.open()

#УДАЛЕНИЕ ДАННЫХ ИЗ ТАБЛИЦЫ
async def removedata():
    row = await mytable.get_selected_row()
    mydata.remove(row)
    
    ui.notify("Удалено", color="red")
    mytable.options['rowData'] = sorted(mydata, key=lambda data:data['name'])
    mytable.update()

#УДАЛЕНИЕ ДАННЫХ ИЗ ТАБЛИЦЫ
async def copydata():
    row = await mytable.get_selected_row()
    mydata.append(row)
    
    ui.notify("Скопирован", color="green")
    mytable.options['rowData'] = sorted(mydata, key=lambda data:data['name'])
    mytable.update()

#СОХРАНЕНИЕ ДАННЫХ В ТАБЛИЦУ
def savedata():
    for d in mydata:
        if d['name'] == selected_data['name']:
            mydata.remove(d)
            ui.notify("Изменено", color="blue")
            dialogedit.close()

    mydata.append({"name":name_edit.value, 
                   "age":age_edit.value, 
                   "fullname":fullname_edit.value, 
                   "phone":phone_edit.value, 
                   "mobilephone":mobilephone_edit.value})
    mytable.options['rowData'] = sorted(mydata, key=lambda data:data['name'])
    mytable.update()

with ui.dialog() as dialogedit:
    with ui.card():
        name_edit = ui.input(label="Подразделение")
        age_edit = ui.input(label="Должность")
        fullname_edit = ui.input(label="ФИО")
        phone_edit = ui.input(label="Номер телефона")
        mobilephone_edit = ui.input(label="Номер сот. тел.")
        ui.button("Сохранить изменения", on_click=savedata)

#ИЗМЕНЕНИЕ ДАННЫХ В ТАБЛИЦЕ
async def editdata():
    global selected_data
    selected_data = await mytable.get_selected_row()

    if selected_data == None:
        ui.notify("Выберите строку для изменения")
        return

    name_edit.value = selected_data['name']
    age_edit.value = selected_data['age']
    fullname_edit.value = selected_data['fullname']
    phone_edit.value = selected_data['phone']
    mobilephone_edit.value = selected_data['mobilephone']
 

    dialogedit.open()



#СОЗДАНИЕ КНОПОК: ДОБАВЛЕНИЕ, УДАЛЕНИЕ, ИЗМЕНЕНИЕ
with ui.row():
    ui.button('Добавить', on_click=lambda e:opendata(e))
    ui.button('Удалить', on_click=removedata)
    ui.button('Изменить', on_click=editdata)
    ui.button('Скопировать', on_click=copydata)

ui.run()